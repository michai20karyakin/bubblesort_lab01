#ifndef BUBBLESORT_HPP
#define BUBBLESORT_HPP

#include <vector>

class BubbleSort
{
private:

    std::vector<int> array;

public:

    BubbleSort();
    BubbleSort(std::vector<int> array);
    BubbleSort(size_t n, int min, int max);

    void sort();
    void print();
    void read();

};

#endif