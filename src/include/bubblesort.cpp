#include <iostream>
#include <cctype>
#include <random>
#include <functional>
#include <sstream>
#include "bubblesort.hpp"
#include <string>

using u32    = uint_least32_t; 
using engine = std::mt19937;

/*
    Конструктор по умолчанию
*/
BubbleSort::BubbleSort()
{

}

/*
    Конструктор класса BubbleSort, принимающий массив (вектор) целых чисел
*/
BubbleSort::BubbleSort(std::vector<int> array)
{
    this->array = array;
}

/*
    Конструктор класса BubbleSort, принимающий количество чисел в массиве 
    и инициализирующий массив случайными числами
*/
BubbleSort::BubbleSort(size_t n, int min, int max)
{
    array.reserve(n);


    std::random_device os_seed;
    const u32 seed = os_seed();

    engine engine_generator( seed );
    std::uniform_int_distribution< u32 > distribute( min, max );

    auto generator = std::bind(distribute, engine_generator);
    std::generate_n(std::back_inserter(array), n, generator); 

}


void BubbleSort::read()
{
    //std::istream_iterator<int> begin(std::cin), end;
    
    //std::vector<int> v(begin, end);


    std::string line = "";
    std::getline(std::cin, line);
    std::istringstream ssin(line);
        
    int input;

    while(ssin >> input)
    {
        array.push_back(input);
    }

}

void BubbleSort::sort() 
{
    for(auto it = array.begin(); it != (array.end() - 1); ++it)
    {
        for(auto jt = array.end() - 1; jt != (it); --jt)
        {
            if (*jt < *(jt - 1))
            {
                int current = *jt;
                *jt = *(jt - 1);
                *(jt - 1) = current;
            }
        }
    }
}


void BubbleSort::print() 
{
    for(auto it = array.begin(); it != array.end(); ++it)
    {
        std::cout << *it << " ";
    }

    std::cout << std::endl;
}